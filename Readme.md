# cactus-proto-validator #

This is an extension to the Cactus testing framework to be able to test and validate protobuffer messages.

It allows you to define endpoints which accept protobuf message:

```
#!xml

<citrus-http:client id="endpoint-to-test"
                      request-url="${endpoint.url}"
                      request-method="POST"
                      content-type="application/x-protobuf"
                      timeout="60000"/>
```
What you can feed with proto message:

```
#!java

ProtoMessage message = new ProtoMessage(RequestProto.Builder.build().toByteArray());
designer.http().client("endpoint-to-test")
        .post()
        .message(message);
```
and later you can validate the proto response with java (here the validator finds out the protobuf message class based on the response type):

```
#!java

ResponseProto response = ResponseProto.newBuilder()....build();
DefaultMessage message = new DefaultMessage(response);

designer.http().client("endpoint-to-test")
        .response(HttpStatus.OK).messageType(ProtoMessage.PROTO_BUFF_MIME_TYPE).message(message);
```
...or you can write groovy script to validate the required fields separately:

```
#!java

String field1Value = "value1"
int field2Value = "3"

//you have to tell the script validator what is the type of the response message
//by putting the class name on the testContext and the groovy template reads this value
designer.variable(ProtoMessage.PROTO_CLASS, ProtoResponse.class.getName());

designer.http().client("endpoint-to-test")
        .response(HttpStatus.OK).messageType(ProtoMessage.PROTO_BUFF_MIME_TYPE)
        .validator("defaultGroovyProtoMessageValidator")
        .validateScript(
            "assert response.field1Value == " + field1Value + ";\n" +
            "assert response.field2Value == '" + field2Value + "';"
         ); 
```