package com.cipur.cactus.protobuf;

import com.consol.citrus.message.Message;
import com.consol.citrus.validation.script.GroovyScriptMessageValidator;
import org.springframework.core.io.ClassPathResource;

/**
 * Groovy script validator for protobuf messages, it holds the groovy template file
 */
public class ProtoMessageScriptValidator extends GroovyScriptMessageValidator {

    public ProtoMessageScriptValidator() {
        super(new ClassPathResource("scripts/proto-validation-template.groovy"));
    }

    @Override
    public boolean supportsMessageType(String messageType, Message message) {
        return messageType.equalsIgnoreCase(ProtoMessage.PROTO_BUFF_MIME_TYPE);
    }

}
