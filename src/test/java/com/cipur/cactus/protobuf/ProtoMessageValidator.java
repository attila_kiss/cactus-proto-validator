package com.cipur.cactus.protobuf;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.exceptions.ValidationException;
import com.consol.citrus.message.Message;
import com.consol.citrus.validation.AbstractMessageValidator;
import com.consol.citrus.validation.context.ValidationContext;
import com.googlecode.protobuf.format.JsonFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

/**
 * General validator for protobuf messages. Based on the control message it tries to find out the protobuf class and tries
 * to call the "parseFrom" message on that class. After that it converts to JSON for comparison and for better
 * display if needed.
 */
public class ProtoMessageValidator extends AbstractMessageValidator<ValidationContext> {

    private static Logger log = LoggerFactory.getLogger(ProtoMessageValidator.class);

    public void validateMessagePayload(Message receivedMessage, Message controlMessage, ValidationContext validationContext, TestContext context) throws ValidationException {
        try {
            JsonFormat jsonFormat = new JsonFormat();
            Object controlProto = controlMessage.getPayload();
            Class protoClazz = controlProto.getClass();
            @SuppressWarnings("unchecked")
            Method parseFrom = protoClazz.getMethod("parseFrom", byte[].class);

            Object responseProto = parseFrom.invoke(null, ((String)receivedMessage.getPayload()).getBytes(StandardCharsets.ISO_8859_1));

            String controlJson = jsonFormat.printToString((com.google.protobuf.Message) controlProto);
            String responseJson = jsonFormat.printToString((com.google.protobuf.Message) responseProto);

            compare(responseJson, controlJson);
        } catch (IllegalArgumentException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e){
            log.error("Validation failed", e);
            throw new ValidationException(e.getMessage());
        }
    }

    @Override
    public boolean supportsMessageType(String s, Message message) {
        return s.equalsIgnoreCase(ProtoMessage.PROTO_BUFF_MIME_TYPE);
    }

    @Override
    protected Class<ValidationContext> getRequiredValidationContextType() {
        return ValidationContext.class;
    }

    /**
     * We check the equality of the to message with simple string comparison. You can customize this method by converting
     * the String params to json object (Map<String, Object>)
     *
     * @param receivedMessageAsJson String representation of the received protobuf message converted to json
     * @param controlMessageAsJson String representation of the control protobuf message converted to json
     * @throws ValidationException
     */
    protected void compare(String receivedMessageAsJson, String controlMessageAsJson) throws ValidationException {
        try{
            Assert.isTrue(controlMessageAsJson.equals(receivedMessageAsJson),
                    String.format("the received message wasn't the same as the control message. Expected %1$s, but was %2$s",
                            controlMessageAsJson, receivedMessageAsJson));
        } catch (IllegalArgumentException e){
            log.error("Validation failed", e);
            throw new ValidationException(e.getMessage());
        }
    }
}
