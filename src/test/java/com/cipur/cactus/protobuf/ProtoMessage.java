package com.cipur.cactus.protobuf;

import com.consol.citrus.http.message.HttpMessage;

/**
 * Simple protobuf payload with the correct header set
 */
public class ProtoMessage extends HttpMessage {

    public static final String PROTO_BUFF_MIME_TYPE = "application/x-protobuf";
    public static final String PROTO_CLASS = "protoClass";

    public ProtoMessage(Object payLoad){
        super(payLoad);
        accept(PROTO_BUFF_MIME_TYPE);
        contentType(PROTO_BUFF_MIME_TYPE);
    }
}
