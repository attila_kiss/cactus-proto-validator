package com.cipur.cactus.protobuf;

import com.consol.citrus.validation.context.ValidationContext;

/**
 * Context for protobuf message validation. We don't store anything in here currently
 * but it's still needed to run the tests properly
 */
public class ProtoMessageValidationContext implements ValidationContext {

}
