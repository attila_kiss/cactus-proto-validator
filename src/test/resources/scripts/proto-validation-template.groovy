
import com.consol.citrus.context.TestContext
import com.consol.citrus.exceptions.CitrusRuntimeException
import com.consol.citrus.message.Message
import com.consol.citrus.validation.script.GroovyScriptExecutor

import java.nio.charset.StandardCharsets

import static com.yoti.connect.api.test.util.protobuf.ProtoMessage.*

class ProtoValidationScript implements GroovyScriptExecutor{

    @Override
    public void validate(Message message, TestContext testContext) {

        def response = null

        try {
            def binaryProto = ((String)message.getPayload()).getBytes(StandardCharsets.ISO_8859_1);
            def className = testContext.getVariable(PROTO_CLASS)
            response = Class.forName(className).parseFrom(binaryProto);
        } catch (Exception e) {
            throw new CitrusRuntimeException("Failed to create protobuf message from payload", e)
        }

        @SCRIPTBODY@

    }
}